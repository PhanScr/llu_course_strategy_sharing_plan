# LLU_Course_Strategy_Sharing_Plan
吕梁学院课程攻略共享计划


#### 介绍Introduction
Inspired by Tsinghua University related projects, we created this project.

受thu相关项目启发，创立了本项目。

When we come to a university, we often collect a lot of information from the first contact with many courses until the completion of one by one. 

我们来到一所大学，第一次接触许多课，直到一门一门完成，这个过程中我们时常收集起许多资料和情报。

Some are e-books that need to be searched on the Internet. Every time I see a new course, I use Google to research the name of the textbook, and some can be found immediately, while some require a lot of vision; some are past test papers or A4 papers. Collecting and making, and making public with ideas that can be useful to others, but it needs to be explored in various groups or in private so that it can be passed on from generation to generation from the hands of seniors; some of the skills that are suddenly understood after finishing a course, the focus of this course is that , It could have been easier and better.

有些是需要在网上搜索的电子书，每次我见到一门新课程，Google 一下教材名称，有的可以立即找到，有的却是要花费许多眼力；有些是历年试卷或者 A4 纸，前人精心收集制作，抱着能对他人有用的想法公开，却需要在各个群或者私下中摸索以至于从学长手中代代相传；有些是上完一门课才恍然领悟的技巧，原来这门课重点如此，当初本可以更轻松地完成得更好。

I have also worked very hard to collect various course materials, but in the end, the acquisition of some important information is often purely accidental. This state often makes me feel scared and uneasy. I also finally had some methods and conclusions after the course, but there was nowhere to tell these thoughts. In the end, I could only dissipate the experience that was spent time and energy in exchange for long forgetting.

我也曾很努力地收集各种课程资料，但到最后，某些重要信息的得到却往往依然是纯属偶然。这种状态时常令我感到后怕与不安。我也曾在课程结束后终于有了些许方法与总结，但这些想法无处诉说，最终只能把花费时间与精力才换来的经验耗散在了漫漫的遗忘之中。

I feel angry and anxietious for the repeated labor of so many people fighting alone throughout the year.

我为这一年一年，这么多人孤军奋战的重复劳动感到不平。

I hope that these difficult, obscure, uncertain, and word-of-mouth information and experiences can be turned into public, easily accessible and shared information that everyone can improve and accumulate together.

我希望能够将这些困难又隐晦、不确定、口口相传的资料和经验，变为公开的、易于获取的和大家能够共同完善、积累的共享资料。

I hope that the detours that the predecessors have walked, the future generations do not have to go.

我希望前人走过的弯路，后人不必再走。

感谢对本项目贡献的同学和电脑前的你！

#### 使用说明(Introduction)
1.  请注意不要上传超过100M的单个文件，否则commit无效。（并没有采用git-lfs）
2.  为了贡献者的安全 请不要特别放纵的公布
3.  禁止上传您未持有版权的文件

#### 参与贡献(Join us!)

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交
4.  新建 Pull Request
5.  上传文件格式：课程号_课程名


#### 特征Feature

1.  课程攻略：readme.md
2.  电子版课件和参考教材：ebook/[教学年份]/[老师姓名].pdf （由于可能存在的版权问题，暂未加入）
3.  平时作业答案：hw/[学生年级_学生姓名]/
4.  历年试卷和复习资料：exam/
5.  开卷考试 A4 纸：a4paper/
6.  欢迎往对应的readme.md里面放自己的GitHub or Gitee对应仓库链接~
7.  形势与政策课程与四大门思政学院课程不提供任何资料，它每个学期都在变，每个学校的内容可能也不太相同，故这些课认真听课即可。另外形势与政策课程需要做好前五学期的笔记，最后一学期开卷考试，不要到时候没得抄。四大门思政学院课程会提供题库，但不能保证是最新的。


电脑前的您请不要吝啬，您的作品一旦被合并至该仓库中，有可能会被无数学弟 or 学妹仰慕。我们需要您的加入！

#### 使用本平台原因Reasons
1.  微信群 or qq群 大多为年级和专业所分隔，无法长期共同地保有；况且群文件也缺乏组织。
2.  这里可以使用目录进行文件组织，并且每个目录均可以在显示文件列表的同时显示一个 README，十分适合本项目的需求。
3.  这里Issue 和 Pull Request协作功能，可以方便地对贡献的质量进行监督和调整。

#### 贡献Contributions!
Issue、PR、纠错、资料、选课/考试攻略，完全欢迎！
来自大家的关注、维护和贡献，才是让这个攻略继续存在的动力~

#### 许可Permition
CC-BY-NC-SA：署名-非商业性使用-相同方式共享
资料仅供参考，请自己判断其适用性。
其他部分的版权归属于其各自的作者。
