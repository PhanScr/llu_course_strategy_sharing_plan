本文写自2021.8.24 作者PhanScr

笔者在焦急的等待着2021CET4成绩（笔者高考SX省，2020年全国一卷，得分120（满分150）。由疫情原因和开学迟，故2021年6月是笔者第一次走上CET4考场。以下介绍笔者的弯路，请勿模仿：

1.毫无复习，只帮着喜欢的妹子复习。这是大忌！这是大忌！这是大忌！（重要的话说三遍！）笔者考前连题目都没有做过一份，听力也马马虎虎，时间分配不足。【这里笔者的一个TIP：请把选词填空放在最后，它就一鸡肋（食之无味 弃之可惜）。所以笔者这个题放弃了，没做，做好其他题目足够确保您可以拿一个满意的分数】

2.平时也没怎么练。只是上课跟着。。但这就出了问题。笔者还是想说一遍“学习是自己的事情，这里由于笔者一年没有碰英语，从而现在开始后悔”

3.听力练习不足。考前至少两个月开始每天坚持听力。这里笔者不强调方法论，方法因人而异。

4.没有刷往年的题目。这里笔者还是建议刷一些模拟题，之后真题拿来练手，近四年真题足够了。严格把控时间，稳过没有问题。

后面是笔者CET4口试考试的一点心得

1.时间不够用，但也别紧张。

2.朗读短文要控制语速，因为笔者读完了之后还有40s。之前有一次预读熟悉，可以跟着读也可以不读，熟悉文章这个不算分。

3.一定要好好准备一个自我介绍模版，控制好时长，考前多排练，尽量做到无意识的情况下说完。

4.最后一部分对话，仔细审题。之后把握好时间，也要掌握好打断别人说话的技巧！！！最后这点很重要，如果为双方好，那么时间分配就是一人一半，你看他差不多了，你可以打断他，当然这里请你善良，不要一直说不给对方机会。不欺负人也不能被人欺负了。

UPD:笔者CET4过了。口语B。笔试马马虎虎，就不说是多少了。